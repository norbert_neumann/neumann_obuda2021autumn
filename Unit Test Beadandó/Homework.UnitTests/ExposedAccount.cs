﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework.UnitTests
{
    public class ExposedAccount : Account
    {
        public ExposedAccount(int id) : base(id)
        {

        }

        public void SetActionsSuccessfullyPerformed(int arg)
        {
            this.ActionsSuccessfullyPerformed = arg;
        }
    }
}
