﻿using Homework.ThirdParty;
using Moq;
using NUnit.Framework;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Homework.UnitTests
{
    [TestFixture]
    public class AccountActivityServiceTests
    {
        private AccountActivityService service;
        private Mock<IAccountRepository> mockAccountRepoProvider;
        private List<ExposedAccount> accounts;

        [SetUp]
        public void SetUp()
        {
            mockAccountRepoProvider = new Mock<IAccountRepository>();
            service = new AccountActivityService(mockAccountRepoProvider.Object);
        }

        private void SetUpFor_GetAmountForActivityMethod()
        {
            accounts = Enumerable.Range(0, 6).Select(i => new ExposedAccount(i)).ToList();

            accounts[0].SetActionsSuccessfullyPerformed(0);
            accounts[1].SetActionsSuccessfullyPerformed(15);
            accounts[2].SetActionsSuccessfullyPerformed(20);
            accounts[3].SetActionsSuccessfullyPerformed(39);
            accounts[4].SetActionsSuccessfullyPerformed(40);
            accounts[5].SetActionsSuccessfullyPerformed(99);

            accounts.ForEach(acc => mockAccountRepoProvider.Setup(
                            provider => provider.Get(acc.Id)).Returns(acc));
            mockAccountRepoProvider.Setup(provider => provider.GetAll()).Returns(accounts);
        }

        [Test]
        public void InvalidAccountId_GetAcitvity_ThrowsAccountNotExistsException()
        {
            // Arrange
            mockAccountRepoProvider.Setup(provider => provider.Get(0)).Returns(new Account(0));

            // Act + Assert
            Assert.Throws<AccountNotExistsException>(() => service.GetActivity(-1));
           // Assert.That(() => service.GetActivity(-1), Throws.TypeOf<AccountNotExistsException>);
        }

        [Test]
        public void ValidAccountId_GetActivity_ProperMethodsCalled()
        {
            // Arrange
            mockAccountRepoProvider.Setup(provider => provider.Get(0)).Returns(new Account(0));

            // Act
            service.GetActivity(0);

            // Assert
            mockAccountRepoProvider.Verify(provider => provider.Get(It.IsAny<int>()), Times.Once);
            mockAccountRepoProvider.VerifyNoOtherCalls();
        }

        [Test]
        public void InvalidAccountId_GetActivity_ProperMethodsCalled()
        {
            // Arrange
            mockAccountRepoProvider.Setup(provider => provider.Get(0)).Returns(new Account(0));

            // "Act"
            Assert.Throws<AccountNotExistsException>(() => service.GetActivity(-1));

            // Assert
            mockAccountRepoProvider.Verify(provider => provider.Get(It.IsAny<int>()), Times.Once);
            mockAccountRepoProvider.VerifyNoOtherCalls();
        }

        [Test]
        public void AccountWithZeroActionsSuccessfullyPerformed_GetActivity_ActivityLevelNone()
        {
            // Arrange
            // Account has 0 successfully performed actions by default
            mockAccountRepoProvider.Setup(provider => provider.Get(0)).Returns(new Account(0));

            // Act
            ActivityLevel level = service.GetActivity(0);

            // Assert
            Assert.That(level, Is.EqualTo(ActivityLevel.None));
        }

        [Test]
        public void AccountWith1To19ActionsSuccessfullyPerformed_GetActivity_ActivityLevelLow([Range(1, 19)]int actionsSuccessfullyPerformed)
        {
            // Arrange
            ExposedAccount acc = new ExposedAccount(0);
            acc.SetActionsSuccessfullyPerformed(actionsSuccessfullyPerformed);
            mockAccountRepoProvider.Setup(provider => provider.Get(0)).Returns(acc);

            // Act
            ActivityLevel level = service.GetActivity(0);

            // Assert
            Assert.That(level, Is.EqualTo(ActivityLevel.Low));
        }

        [Test]
        public void AccountWith20To39ActionsSuccessfullyPerformed_GetActivity_ActivityLevelMedium([Range(20, 39)] int actionsSuccessfullyPerformed)
        {
            // Arrange
            ExposedAccount acc = new ExposedAccount(0);
            acc.SetActionsSuccessfullyPerformed(actionsSuccessfullyPerformed);
            mockAccountRepoProvider.Setup(provider => provider.Get(0)).Returns(acc);

            // Act
            ActivityLevel level = service.GetActivity(0);

            // Assert
            Assert.That(level, Is.EqualTo(ActivityLevel.Medium));
        }

        [Test]
        public void AccountWithMoreThan39ActionsSuccessfullyPerformed_GetActivity_ActivityLevelHigh([Range(40, 100, 10)] int actionsSuccessfullyPerformed)
        {
            // Arrange
            ExposedAccount acc = new ExposedAccount(0);
            acc.SetActionsSuccessfullyPerformed(actionsSuccessfullyPerformed);
            mockAccountRepoProvider.Setup(provider => provider.Get(0)).Returns(acc);

            // Act
            ActivityLevel level = service.GetActivity(0);

            // Assert
            Assert.That(level, Is.EqualTo(ActivityLevel.High));
        }
    
        [Test]
        [TestCase(ActivityLevel.None, 1)]
        [TestCase(ActivityLevel.Low, 1)]
        [TestCase(ActivityLevel.Medium, 2)]
        [TestCase(ActivityLevel.High, 2)]
        public void AccountAcitvityService_GetAmountForActivityNone_ReturnsExpectedValues(ActivityLevel level, int expected)
        {
            // Arrange
            SetUpFor_GetAmountForActivityMethod();

            // Act
            int result = service.GetAmountForActivity(level);

            // Assert
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void AccountAcitvityService_GetAmountForActivity_ProperMethodsCalled()
        {
            // Arrange
            SetUpFor_GetAmountForActivityMethod();

            // Act
            int result = service.GetAmountForActivity(ActivityLevel.None);

            // Assert
            mockAccountRepoProvider.Verify(provider => provider.GetAll(), Times.Once);
            mockAccountRepoProvider.Verify(provider => provider.Get(It.IsAny<int>()), Times.Exactly(accounts.Count));
            mockAccountRepoProvider.VerifyNoOtherCalls();
        }


    }
}
