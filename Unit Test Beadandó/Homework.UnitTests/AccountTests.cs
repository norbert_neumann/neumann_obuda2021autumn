﻿using Homework.ThirdParty;
using Moq;
using NUnit.Framework;
using System;

namespace Homework.UnitTests
{
    [TestFixture]
    public class AccountTests
    {
        private Mock<IAction> mockActionProvider;

        [SetUp]
        public void SetUp()
        {
            mockActionProvider = new Mock<IAction>();
        }

        // körülmény_akció_eredmény

        //[Test]
        [TestCase(int.MinValue)]
        [TestCase(0)]
        [TestCase(int.MaxValue)]
        public void Constructor_GetId_ReturnsTheSetId(int id)
        {
            // Arrange
            Account acc = new Account(id);

            // Act
            int result = acc.Id;

            // Assert
            Assert.That(result, Is.EqualTo(id));
        }

        [Test]
        public void RegisteredAccount_IsRegistered_True()
        {
            // Arrange
            Account acc = new Account(0);
            acc.Register();

            // Act
            bool result = acc.IsRegistered;

            // Assert
            Assert.That(result, Is.True);
        }

        [Test]
        public void ActivatedAccount_IsConfirmed_True()
        {
            // Arrange
            Account acc = new Account(0);
            acc.Activate();

            // Act
            bool result = acc.IsConfirmed;

            // Assert
            Assert.That(result, Is.True);
        }

        [Test]
        public void ValidAccount_TakeAction_ThrowsNoException()
        {
            // Arrange
            Account acc = new Account(0);
            acc.Activate();
            acc.Register();

            // Act + Assert
            Assert.That(() => acc.TakeAction(mockActionProvider.Object),
                        Throws.Nothing);
        }

        [Test]
        public void InactiveAccount_TakeAction_ThrowsInactiveUserException()
        {
            // Arrange
            Account acc = new Account(0);

            // Act + Assert
            Assert.That(() => acc.TakeAction(mockActionProvider.Object),
                        Throws.TypeOf<InactiveUserException>());
        }

        [Test]
        public void ActivatedAccount_TakeAction_ThrowsNoException()
        {
            // Arrange
            Account acc = new Account(0);
            acc.Activate();

            // Act + Assert
            Assert.That(() => acc.TakeAction(mockActionProvider.Object),
                        Throws.Nothing);
        }

        [Test]
        public void RegisteredAccount_TakeAction_ThrowsNoException()
        {
            // Arrange
            Account acc = new Account(0);
            acc.Register();

            // Act + Assert
            Assert.That(() => acc.TakeAction(mockActionProvider.Object),
                        Throws.Nothing);
        }

        [Test]
        public void VanillaAccount_TakeAction_ThrowsInactiveUserException()
        {
            // Arrange
            Account acc = new Account(0);

            // Act + Assert
            Assert.That(() => acc.TakeAction(mockActionProvider.Object),
                        Throws.TypeOf<InactiveUserException>());
        }

        [Test]
        public void ValidAccount_TakeAction_ExecuteMethodGetsCalledOnce()
        {
            // Arrange
            Account acc = new Account(0);
            acc.Activate();
            acc.Register();

            // Act
            bool result = acc.TakeAction(mockActionProvider.Object);

            // Assert
            mockActionProvider.Verify(provider => provider.Execute(), Times.Once);
        }

        [Test]
        public void InvalidAccount_TakeAction_ExecuteMethodIsntCalled()
        {
            // Arrange
            Account acc = new Account(0);

            // "Act"
            Assert.That(() => acc.TakeAction(mockActionProvider.Object),
                        Throws.TypeOf<InactiveUserException>());

            // Assert
            mockActionProvider.Verify(provider => provider.Execute(), Times.Never);
        }

        [Test]
        public void SuccesfullActionExecution_PerformAction_IncrementsActionsSuccessfullyPerformedProperty()
        {
            // Arrange
            Account acc = new Account(0);
            acc.Activate();
            mockActionProvider.Setup(provider => provider.Execute()).Returns(true);

            // Act
            acc.TakeAction(mockActionProvider.Object);

            // Assert
            Assert.That(acc.ActionsSuccessfullyPerformed, Is.EqualTo(1));
        }

        // Ez lehet nem a legjobb
        [Test]
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(10)]
        public void MultipleSuccesfullActionExecution_PerformAction_IncrementsActionsSuccessfullyPerformedProperty(int succesfullActionCount)
        {
            // Arrange
            Account acc = new Account(0);
            acc.Activate();
            mockActionProvider.Setup(provider => provider.Execute()).Returns(true);

            // Act
            for (int i = 0; i < succesfullActionCount; i++)
            {
                acc.TakeAction(mockActionProvider.Object);
            }

            // Assert
            Assert.That(acc.ActionsSuccessfullyPerformed, Is.EqualTo(succesfullActionCount));
        }

        [Test]
        public void UnsuccesfullActionExecution_PerformAction_DoesntIncrementActionsSuccessfullyPerformedProperty()
        {
            // Arrange
            Account acc = new Account(0);
            acc.Activate();
            mockActionProvider.Setup(provider => provider.Execute()).Returns(false);

            // Act
            acc.TakeAction(mockActionProvider.Object);

            // Assert
            Assert.That(acc.ActionsSuccessfullyPerformed, Is.EqualTo(0));
        }

        [Test]
        public void SuccesfullActionExecution_PerformAction_True()
        {
            Account acc = new Account(0);
            acc.Activate();
            mockActionProvider.Setup(provider => provider.Execute()).Returns(true);

            // Act
            bool result = acc.TakeAction(mockActionProvider.Object);

            // Assert
            Assert.That(result, Is.EqualTo(true));
        }

        [Test]
        public void UnsuccesfullActionExecution_PerformAction_False()
        {
            Account acc = new Account(0);
            acc.Activate();
            mockActionProvider.Setup(provider => provider.Execute()).Returns(false);

            // Act
            bool result = acc.TakeAction(mockActionProvider.Object);

            // Assert
            Assert.That(result, Is.EqualTo(false));
        }
    }
}
