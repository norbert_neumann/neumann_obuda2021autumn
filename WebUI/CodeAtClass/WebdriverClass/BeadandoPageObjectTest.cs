﻿using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass
{
    class BeadandoPageObjectTest : TestBase
    {
        [Test, TestCaseSource("GetTestData")]
        public void ArXivTest(string method, string expectedUrl)
        {
            LandingPage landing = LandingPage.Navigate(Driver);
            MethodInfo getterMethod = typeof(LandingPage).GetMethod(method);
            BasePage page = getterMethod.Invoke(landing, null) as BasePage;

            Assert.AreEqual(expectedUrl, page.GetUrl());
            page.GetElements().ForEach(webElement => Assert.IsTrue(webElement.Displayed));
        }

        static IEnumerable GetTestData()
        {
            var doc = XElement.Load(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory) + "\\data.xml");
            return
                from vars in doc.Descendants("testData")
                let methodName = vars.Attribute("methodName").Value
                let url = vars.Attribute("url").Value
                select new object[] { methodName, url };
        }
    }
}
