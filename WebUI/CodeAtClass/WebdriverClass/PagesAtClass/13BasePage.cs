﻿using OpenQA.Selenium;
using System.Collections;
using System.Collections.Generic;

namespace WebdriverClass.PagesAtClass
{
    public abstract class BasePage
    {
        protected IWebDriver Driver;

        public BasePage(IWebDriver webDriver)
        {
            Driver = webDriver;
        }

        public string GetUrl()
        {
            return Driver.Url;
        }

        public abstract List<IWebElement> GetElements();
    }
}
