﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace WebdriverClass.PagesAtClass
{
    public class LandingPage : BasePage
    {
        private IWebElement SearchButton => Driver.FindElement(By.CssSelector("input[id='adv-search-btn']"));
        private IWebElement AstroLink =>  Driver.FindElement(By.Id("main-astro-ph"));
        private IWebElement HelpLink => Driver.FindElement(By.CssSelector("a[href='https://arxiv.org/help']"));

        public LandingPage(IWebDriver driver) : base(driver)
        {
        }

        public static LandingPage Navigate(IWebDriver webDriver)
        {
            webDriver.Url = "https://arxiv.org/";
            return new LandingPage(webDriver);
        }

        public PhysicsSearchPage GetPhysicsSearchPage()
        {
            SearchButton.Click();
            return new PhysicsSearchPage(Driver);
        }

        public AstrophysicsPage GetAstrophysicsPage()
        {
            AstroLink.Click();
            return new AstrophysicsPage(Driver);
        }

        public HelpPage GetHelpPage()
        {
            HelpLink.Click();
            return new HelpPage(Driver);
        }

        public override List<IWebElement> GetElements()
        {
            return new List<IWebElement>()
            {
                SearchButton,
                AstroLink,
                HelpLink
            };
        }
    }
}
