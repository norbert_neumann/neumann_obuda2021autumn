﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverClass.PagesAtClass
{
    public class AstrophysicsPage : BasePage
    {
        private IWebElement browseForm => Driver.FindElement(By.CssSelector("form[action='/list']"));
        private IWebElement catchUpForm => Driver.FindElement(By.CssSelector("form[action='/catchup']"));
        private IWebElement linkToAstroPhArchive => Driver.FindElement(By.CssSelector("a[href='https://arxiv.org/search/astro-ph']"));

        public AstrophysicsPage(IWebDriver webDriver) : base(webDriver)
        {

        }

        public override List<IWebElement> GetElements()
        {
            return new List<IWebElement>()
            {
                browseForm,
                catchUpForm,
                linkToAstroPhArchive
            };
        }
    }
}
