﻿using OpenQA.Selenium;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverClass.PagesAtClass
{
    public class PhysicsSearchPage : BasePage
    {
        public IWebElement searchBar => Driver.FindElement(By.CssSelector("input[id='query']"));
        public IWebElement searchType => Driver.FindElement(By.CssSelector("select[id='searchtype']"));
        public IWebElement searchButton => Driver.FindElement(By.CssSelector("button[class='button is-link is-medium']"));
        public IWebElement seachByAutoherField => Driver.FindElement(By.CssSelector("article[class='message is-link']"));
        public IWebElement showAbstractsRadionBtn => Driver.FindElement(By.Id("abstracts-0"));
        public IWebElement hideAbstractsRadionBtn => Driver.FindElement(By.Id("abstracts-1"));

        public PhysicsSearchPage(IWebDriver driver) : base(driver)
        {

        }

        public static PhysicsSearchPage Navigate(IWebDriver webDriver)
        {
            webDriver.Url = "https://arxiv.org/search/physics";
            return new PhysicsSearchPage(webDriver);
        }

        public override List<IWebElement> GetElements()
        {
            return new List<IWebElement>()
            {
                searchBar,
                searchType,
                searchButton,
                seachByAutoherField,
                showAbstractsRadionBtn,
                hideAbstractsRadionBtn
            };
        }
    }
}
