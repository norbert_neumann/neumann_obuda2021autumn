﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverClass.PagesAtClass
{
    public class HelpPage : BasePage
    {
        private IWebElement searchTextBox => Driver.FindElement(By.Id("q"));
        private IWebElement searchButton => Driver.FindElement(By.CssSelector("button[class='button is-link is-small']"));
        private IWebElement mainHeader => Driver.FindElement(By.Id("arxiv-help-contents"));
        private IWebElement aboutLink => Driver.FindElement(By.LinkText("About arXiv"));

        public HelpPage(IWebDriver webDriver) : base(webDriver)
        {

        }

        public override List<IWebElement> GetElements()
        {
            return new List<IWebElement>()
            {
                searchTextBox,
                searchButton,
                mainHeader,
                aboutLink
            };
        }
    }
}
